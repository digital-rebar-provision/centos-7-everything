# Centos 7 Everything

* [Introduction](#introduction)
* [Quickstart](#quickstart)
* [Modification](#modification)
* [Troubleshooting](#troubleshooting)

## Introduction

For [Digital Rebar Provision](http://rebar.digital), which is maintained by
[RackN](https://rackn.com).

This content package provides a workflow, bootenv, kickstart template and stages and tasks to use in the installation of a CentOS 7 machine. The boot environment uses a full ISO to host and serve CentOS packages on the drp server to expedite installation and make the spectrum of installation options available, namely, installation of a GNOME GUI.

## Quick Start
Issue the following commands from your drp server:

1. git clone and cd into the content directory:
`cd content`
2. Build the bundle; outputs centos-7-everything.yml to the root of the git project.
`drpcli contents bundle ../centos-7-everything.yml`
3. Upload the bundle to the drp endpoint
`drpcli contents create ../centos-7-everything.yml`
4. Upload and explode the ISO so that the kernel, initrd, packages etc are made available from the DRP server for the installation
`drpcli bootenvs uploadiso centos-7-everything-install`
5. Discover a Machine and throw the workflow against it!

![Quickstart](images/quickstart.gif "Quick Start")

## Modification
You can easily configure the provided yaml and kickstart template to suite your needs. After editing them to your satisfaction, you can update the content pack in place by:
`drpcli contents bundle ../centos-7-everything.yml`
and
`drpcli contents update centos-7-everything ../centos-7-everything.yml`

Content bundles are meant to be read-only; if you find yourself wanting to edit them (for example, editing the select-kickseed param to point at a cloned kickseed template that you named something else), you can convert the bundle into individual parts in DRP's data store to make them writable. By doing this, you can no longer manage the components as a bundle; you'd have to remove the bootenv, stage, workflow and template separately if you wanted to remove all of the content.

Uploading and breaking up the bundle into separate (and editable) parts:
`drpcli contents convert ../centos-7-everything.yml`

## Troubleshooting
The 'bootenvs uploadiso' step from step 4 of the quickstart should have done the following:
1. Made an ISO file named `CentOS-7-x86_64-Everything-2009.iso` into the /path/to/drp-dir/tftpboot/isos/ directory, where tftpboot is relative to the DRP installation directory (I.E. /var/lib/dr-provision/tftpboot/isos/CentOS-7-x86_64-Everything-2009.iso)
2. Unpacked the ISO into /path/to/drp-dir/tftpboot/centos-7-everything/install/ directory

If the ISO did not explode into tftpboot (causing PXE boot to fail to find the initrd/kernel), you can use the following to manually unpack it if you're in the tftpboot directory:
`./explode_iso.sh centos-7-everything /var/lib/dr-provision/tftpboot/ /var/lib/dr-provision/tftpboot/isos/CentOS-7-x86_64-Everything-2009.iso /var/lib/dr-provision/tftpboot/centos-7-everything/install 689531cce9cf484378481ae762fae362791a9be078fda10e4f6977bf8fa71350`

If your `drpcli contents create` command did not successfully upload the .yml bundle, try creating the bundle in JSON format with the following command (.json extension signals drpcli to output the bundle in json format):

`drpcli contents bundle ../centos-7-everything.json`

Then run the similar following command to upload the .json bundle:
`drpcli contents create ../centos-7-everything.json`
