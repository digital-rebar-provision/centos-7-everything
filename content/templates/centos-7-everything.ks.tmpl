# DigitalRebar Provision Centos-7-Everything kickstart

{{range .InstallRepos}}
{{ .Install }}
{{end}}
# key --skip
# Disable geolocation for language and timezone
# Currently broken by https://bugzilla.redhat.com/show_bug.cgi?id=1111717
# geoloc 0
timezone America/New_York
lang en_US.UTF-8
keyboard us
# rebar
rootpw --iscrypted {{if .ParamExists "provisioner-default-password-hash"}}{{.Param "provisioner-default-password-hash"}}{{else}}$6$drprocksdrprocks$upAIK9ynEEdFmaxJ5j0QRvwmIu2ruJa1A1XB7GZjrnYYXXyNr4qF9FttxMda2j.cmh.TSiLgn4B/7z0iSHkDC1{{end}}
firewall --enabled
authconfig --enableshadow --passalgo=sha512
firstboot --disable
selinux --enforcing
xconfig --defaultdesktop=GNOME --startxonboot

bootloader --location=mbr --driveorder={{.Param "operating-system-disk"}} --append=" crashkernel=auto rhgb quiet"
zerombr
ignoredisk --only-use={{.Param "operating-system-disk"}}
clearpart --all --drives={{.Param "operating-system-disk"}}
part /boot --fstype="ext4" --size=512 --ondisk={{.Param "operating-system-disk"}}
part /boot/efi --fstype="vfat" --size=512 --ondisk={{.Param "operating-system-disk"}}
part pv.01 --size=1 --grow --ondisk={{.Param "operating-system-disk"}}
volgroup centos --pesize=4096 pv.01
logvol /home  --fstype="ext4" --percent=75 --name=home --vgname=centos
logvol swap  --fstype="swap" --recommended --name=swap --vgname=centos
logvol /  --fstype="ext4" --size=1 --grow --name=root --vgname=centos
text
reboot {{if .Param "kexec-ok" }}--kexec{{end}}

%packages
@core
trousers
fipscheck
device-mapper-multipath
openssh
curl
efibootmgr
tar
{{if .ParamExists "extra-packages" -}}
{{ range $index, $element := (.Param "extra-packages") -}}
{{$element}}
{{end -}}
{{end -}}
%end
%post

exec > /root/post-install.log 2>&1
set -x
export PS4='${BASH_SOURCE}@${LINENO}(${FUNCNAME[0]}): '

{{template "reset-workflow.tmpl" .}}
{{template "runner.tmpl" .}}

sync
%end
